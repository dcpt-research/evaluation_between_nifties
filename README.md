# Evaluation for segmentation accuracy

Evaluation between two binary nifty files.

    default_metrics = [
        "Dice",
        "Surface Dice 1mm",
        "Surface Dice 3mm",
        "Hausdorff Distance 95",
        "Avg. Surface Distance",
        "Precision",
        "Recall",
        "False Positive Rate",
        "Jaccard",
        "Accuracy",
        "False Omission Rate",
        "Negative Predictive Value",
        "False Negative Rate",
        "True Negative Rate",
        "False Discovery Rate",
        "Total Positives Test",
        "Total Positives Reference"
    ]


## Getting started

Install requirements


Before installation, please check "requirements.txt" to see if there are any coflicts with your current python environment.
Otherwise, preferably install it in a virtual environment. 

```
$ git clone https://github.com/deepmind/surface-distance.git
$ pip install surface-distance/
$ git clone https://gitlab.com/dcpt-research/evaluation_between_nifties.git
$ cd evaluation_between_nifties
$ pip install -r requirements.txt
```
Part of the code were borrowed from nnUNet.

## Usesage: 
useage example:
```
python evaluator.py -ref /data/ground/truth/nifty/ -pred /data/predicted/nifty/ -l 1
```
Arguments:
```
'-ref', required=True, type=str, help="Folder containing the reference segmentations in nifti "
                                                              "format.")
'-pred', required=True, type=str, help="Folder containing the predicted segmentations in nifti "
                                                               "format. File names must match between the folders!")
'-l', nargs='+', type=int, required=True, help="List of label IDs (integer values) that should "
                                                                       "be evaluated. Best practice is to use all int "
                                                                       "values present in the dataset, so for example "
                                                                       "for LiTS the labels are 0: background, 1: "
                                                                       "liver, 2: tumor. So this argument "
                                                                       "should be -l 1 2. You can if you want also "
                                                                       "evaluate the background label (0) but in "
                                                                       "this case that would not gie any useful "
                                                                       "information.")
```

### Evaluation results would be saved in the prediction folder with name "summary.json"

